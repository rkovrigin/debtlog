//
//  MasterViewController.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 14.02.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
//#import "DebtorDetail.h"
//#import "DebtDetail0.h"
#import "DebtorDetail0.h"
#import "DebtCell.h"
#import "CurrencyRelation.h"

@interface MasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation MasterViewController
@synthesize detailViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        self.title = NSLocalizedString(@"Debtors", @"Debtors");
        self.title = NSLocalizedString(@"TITLE_DEBTORS", nil);
//        NSLog(@"Language: %@", [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0]);
//        NSLog(@"localeIdentifier: %@", [[NSLocale currentLocale] localeIdentifier]);
//        NSLog(@"Region: %@", [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode]);
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;


    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    db = [DBmanager getSharedInstance];
    debtIDs = [db getIDs:@"debt"];
    _debtors = [db getIDs:@"debtor"];
    currencyIDs = [db getRawWithQuery:@"select distinct currency from debt"];
    currencySC.selectedSegmentIndex = [[db getStr:@"select segment from currency where def = 1"] intValue];
    [currencySC addTarget:self action:@selector(setCurrency) forControlEvents:UIControlEventValueChanged];
}

- (void) setCurrency
{
    db = [DBmanager getSharedInstance];
    int oldDefSegment = [[db getStr:@"select segment from currency where def = 1"] intValue];
    @try{
        [db Exec:[NSString stringWithFormat:@"update currency set def = 0"]];
        [db Exec:[NSString stringWithFormat:@"update currency set def = 1 where segment = %d", currencySC.selectedSegmentIndex]];
    }@catch(...){
        [db Exec:[NSString stringWithFormat:@"update currency set def = 1 where segment = %d", oldDefSegment]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    DebtorDetail0 *debtordetail = [[DebtorDetail0 alloc] init];
    
    [delegate.navigationController pushViewController:debtordetail animated:YES];
}

-(void)editButtonItem:(id)sender
{
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    db = [DBmanager getSharedInstance];
    _debtors = [db getIDs:@"debtor"];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    switch (indexPath.section) {
        case 0:
            if (self.editing){
            }else{
                cell.editingAccessoryType = UITableViewCellAccessoryNone;
                cell.textLabel.text = [db getStr:[NSString stringWithFormat: @"select name from debtor where id = %@", [_debtors objectAtIndex:indexPath.row]]];
//                NSLog(@"cell.textLabel.text %@", cell.textLabel.text);
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
        break;
        case 1:
        {
            NSString *symbol = NSLocalizedString(([db getStr:[NSString stringWithFormat:@"select name from currency where segment = %@", [currencyIDs objectAtIndex:indexPath.row]]]), nil);
            NSString *relation = @"";
            NSString *sum = [db getStr:[NSString stringWithFormat:@"select sum(amount) from debt where currency = %@", [currencyIDs objectAtIndex:indexPath.row]]];
            if ([sum intValue] > 0){
                relation = NSLocalizedString(@"RETURN_FROM_YOU", nil);
            }else if ([sum intValue] < 0){
                relation = NSLocalizedString(@"RETURN_TO_YOU", nil);
            }

           cell.textLabel.text = [NSString stringWithFormat:@"%@ %d %@", relation, abs([sum intValue]), symbol];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
            break;
        case 2:
        {
            cell = currencyCell;
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0){
        return YES;
    }
    
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    db = [DBmanager getSharedInstance];
    _debtors = [db getIDs:@"debtor"];
    
    if (indexPath.section == 0){
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            [db Exec:[NSString stringWithFormat:@"delete from debt where debtor = %@", [_debtors objectAtIndex:indexPath.row]]];
            [db Exec:[NSString stringWithFormat:@"delete from debtor where id = %@", [_debtors objectAtIndex:indexPath.row]]];
            _debtors = [db getIDs:@"debtor"];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            currencyIDs = [db getRawWithQuery:@"select distinct currency from debt"];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.detailViewController) {
        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    }
    
    db = [DBmanager getSharedInstance];
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    currencyIDs = [db getRawWithQuery:@"select distinct currency from debt"];

    if (indexPath.section == 0){
        _debtors = [db getIDs:@"debtor"];
        DetailViewController *detail = [[DetailViewController alloc] initWithID:[_debtors objectAtIndex:indexPath.row]];
        [delegate.navigationController pushViewController:detail animated:YES];
    } else if (indexPath.section == 1){
            currencyIDs = [db getRawWithQuery:@"select distinct currency from debt"];
            CurrencyRelation *curRel = [[CurrencyRelation alloc] initWithID:[currencyIDs objectAtIndex:indexPath.row]];
            [delegate.navigationController pushViewController:curRel animated:YES];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	NSString *sectionHeader = nil;
	switch (section){
        case 0: sectionHeader = NSLocalizedString(@"GROUP_DEBTORS", nil);
            break;
        case 1: sectionHeader = NSLocalizedString(@"GROUP_DEBT_RESULT", nil);
            break;
        case 2: sectionHeader = NSLocalizedString(@"GROUP_DEFAULT_CURRENCY", nil);
            break;
        default: break;
    }
	return sectionHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    db = [DBmanager getSharedInstance];
    switch (section){
        case 0:
        {
            _debtors = [db getIDs:@"debtor"];
            return [_debtors count];
        }
            break;
        case 1:
            return [currencyIDs count];
            break;
        case 2:
            return 1;
            break;
        default:
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tv heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if( (indexPath.section == 0) || (indexPath.section == 1) )
    {
        height = 44;
    } else {
        height = 29;
    }
    
    return height;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    db = [DBmanager getSharedInstance];
    _debtors = [db getIDs:@"debtor"];
    currencyIDs = [db getRawWithQuery:@"select distinct currency from debt"];
    [self.tableView reloadData];
}

@end
