//
//  PRLabel.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 03.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "PRLabel.h"

@implementation PRLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)isUserInteractionEnabled
{
    return YES;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.dp = [[UIDatePicker alloc] init];
    self.dp.locale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale currentLocale] localeIdentifier]];
    if (self.dp == nil){
       self.dp = [[UIDatePicker alloc] init];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSDate *anyDate = [dateFormat dateFromString:self.text];
    
    self.dp.date = anyDate;
    
    self.dp.datePickerMode = UIDatePickerModeDate;
    self.inputView = self.dp;
    [self.dp addTarget:self action:@selector(updateLabelFromPicker:) forControlEvents:UIControlEventValueChanged];
}

- (IBAction)updateLabelFromPicker:(id)sender {
    NSDate *date = self.dp.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSString *prettyVersion = [dateFormat stringFromDate:date];
    self.text = prettyVersion;
}

# pragma mark - UIResponder overrides

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self becomeFirstResponder];
}


@end
