//
//  CurrencyRelation.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 15.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBmanager.h"

@interface CurrencyRelation : UIViewController
<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet NSMutableArray *debts;
    IBOutlet NSMutableArray *debtors;
    DBmanager *db;
}
- (id)initWithID:(NSString *)currencyID;
@property NSString *curID;
@property (weak, nonatomic) IBOutlet UITableView *debtsTable;
@end
