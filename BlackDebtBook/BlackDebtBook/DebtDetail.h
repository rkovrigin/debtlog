//
//  DebtDetail.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 18.08.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRLabel.h"
#import "DBmanager.h"

@interface DebtDetail : UIViewController
<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *debtorTablle;
    IBOutlet UITableViewCell *dealTypy;
    IBOutlet UITableViewCell *amount;
    IBOutlet UITableViewCell *startDate;
    IBOutlet UITableViewCell *returnDate;
    IBOutlet UITableViewCell *description;
    IBOutlet UITableViewCell *debtCurryncy;
    IBOutlet UIDatePicker *dp;
    IBOutlet UITextField *fieldAmount;
    IBOutlet PRLabel *fieldStartDate;
    IBOutlet PRLabel *fieldReturnDate;
    IBOutlet NSMutableArray *dsections;
    IBOutlet NSMutableArray *dealTypeSections;
    IBOutlet UISegmentedControl *currencySC;
    IBOutlet UITextView *fieldDescription;
    DBmanager *db;
}
@property (strong, nonatomic) IBOutlet UIDatePicker *DatePicker;
- (IBAction)dateChanged:(id)sender;
- (IBAction)doneEditing:(id)sender;
@end
