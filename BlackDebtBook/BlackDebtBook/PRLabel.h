//
//  PRLabel.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 03.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRLabel : UILabel

@property (strong, nonatomic, readwrite) UIView* inputView;
@property (strong, nonatomic, readwrite) UIView* inputAccessoryView;
@property UIDatePicker* dp;

@end
