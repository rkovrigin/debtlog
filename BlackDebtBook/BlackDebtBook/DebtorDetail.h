//
//  DebtorDetail.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 21.08.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebtorDetail : UIViewController
<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableViewCell *dName;
    IBOutlet UITableViewCell *dCharact;
    IBOutlet UITableView *debtorTable;
//IBOutlet UITableView *debtorTable;
}
//@property (weak, nonatomic) IBOutlet UITextField *DebtoName;

@end
