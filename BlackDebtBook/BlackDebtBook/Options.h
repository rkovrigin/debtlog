//
//  Options.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 15.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface Options : UIViewController
<UITableViewDataSource, UITableViewDelegate>{
    
    IBOutlet UITableViewCell *currencyCell;
    IBOutlet UISegmentedControl *currencySC;
}

@end
