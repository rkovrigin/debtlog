//
//  DebtDetail.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 18.08.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "DebtDetail.h"
#import "DBmanager.h"

@interface DebtDetail ()

@end

@implementation DebtDetail
//@synthesize DebtAmount;
//@synthesize DatePicker;
//@synthesize StartDate;
//@synthesize ReturnDate;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    DBmanager *db = [DBmanager getSharedInstance];
    
    dsections = [[NSMutableArray alloc] init];
    [dsections addObject:amount];
    [dsections addObject:startDate];
    [dsections addObject:returnDate];
    
    dealTypeSections = [[NSMutableArray alloc] init];
    [dealTypeSections addObject:dealTypy];
    [dealTypeSections addObject:debtCurryncy];
    
    currencySC.selectedSegmentIndex = [[db getStr:@"select segment from currency where def = 1"] intValue];
    
    dp.datePickerMode = UIDatePickerModeDate;
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate * currentDate = [NSDate date];
    NSDateComponents * comps = [[NSDateComponents alloc] init];
    [comps setYear: +100];
    NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    [comps setYear: 0];
    NSDate * minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    dp.minimumDate = minDate;
    dp.maximumDate = maxDate;
    dp.date = minDate;
    dp.backgroundColor = [UIColor blackColor];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    fieldStartDate.text = [dateFormat stringFromDate: minDate];
    
    NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
    [dateComponents setMonth:1];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:minDate options:0];
    fieldReturnDate.text = [dateFormat stringFromDate: newDate];
    
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(addDebt:)];
    fieldAmount.keyboardType = UIKeyboardTypeNumberPad;
}

- (void)addDebt:(id)sender
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dateChanged:(id)sender {
//    UIDatePicker *picker = (UIDatePicker *)sender;
    
//    StartDate.text = [NSString stringWithFormat:@"%@", picker.date];
//    ReturnDate.text = [NSString stringWithFormat:@"%@", picker.date];
}

- (IBAction)doneEditing:(id)sender {
//    [StartDate resignFirstResponder];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    NSLog(@"indexPath.row %d", indexPath.row);
    NSLog(@"indexPath.section %d", indexPath.section);

    if (indexPath.section == 0){
        cell = dealTypy;
//        currencySC.frame = cell.frame;
//        currencySC.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }else if (indexPath.section == 1){
        cell = debtCurryncy;
//        currencySC.frame = cell.frame;
//        currencySC.autoresizingMask = UIViewAutoresizingFlexibleWidth;        
    }else if (indexPath.section == 2){
        cell = [dsections objectAtIndex:indexPath.row];
    }else if(indexPath.section == 3){
        cell = description;
    }
//    }else if(indexPath.section == 3){
//        cell = description;
//    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tv heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if( indexPath.section != 3 )
    {
        height = 44;
    } else {
        height = 145;
    }

    if ( indexPath.section == 3 ){
        height = 145;
    }else if (indexPath.section == 0 || indexPath.section == 1){
        height = 40;
    }else{
        height = 44;
    }
    
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	NSArray *sectionArray;
	sectionArray =  [NSArray arrayWithObjects:@"Deal type", @"Deal", @"Currency", @"Description", nil];
	
	return [sectionArray count];	
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	NSString *sectionHeader = nil;
	switch (section){
        case 0: sectionHeader = @"Deal type";
            break;
        case 1: sectionHeader = @"Currency";
            break;
        case 2: sectionHeader = @"Deal data";
            break;
        case 3: sectionHeader = @"Description";
            break;
        default: break;
    }
	return sectionHeader;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"section %d", section);
    switch (section){
        case 0: return 1;
            break;
        case 1: return 1;
            break;
        case 2: return 3;
            break;
        case 3: return 1;
            break;
        default:
            break;
    }
    
    return 0;
}

//-(id) initWithNibName:(NSString*)nibNameOrNil bundle:nibBundleOrNil {
//    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardDidShowNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHidden:) name:UIKeyboardDidHideNotification object:nil];
//    }
//    return self;
//}
//
//-(void) dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
//}
//
//-(void) keyboardShown:(NSNotification*) notification {    
//    CGRect initialFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    CGRect convertedFrame = [self.view convertRect:initialFrame fromView:nil];
//    CGRect tvFrame = debtorTablle.frame;
//    tvFrame.size.height = convertedFrame.origin.y;
//    debtorTablle.frame = tvFrame;
//}
//
//-(void) keyboardHidden:(NSNotification*) notification {
//    int _initialTVHeight = debtorTablle.frame.size.height;
//    CGRect tvFrame = debtorTablle.frame;
//    tvFrame.size.height = _initialTVHeight;
//    [UIView beginAnimations:@"TableViewDown" context:NULL];
//    [UIView setAnimationDuration:0.3f];
//    debtorTablle.frame = tvFrame;
//    [UIView commitAnimations];
//}
//
//-(void) scrollToCell:(NSIndexPath*) path {
//    [debtorTablle scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionNone animated:YES];
//}
//
//-(void) textFieldDidBeginEditing:(UITextField *)textField {
//    NSIndexPath* path = [NSIndexPath indexPathForRow:0 inSection:2];
//    if (textField == fieldAmount){
//        path = [NSIndexPath indexPathForRow:0 inSection:1];
//    }else{
//        path = [NSIndexPath indexPathForRow:0 inSection:3];
//    }
//    [self performSelector:@selector(scrollToCell:) withObject:path afterDelay:0.5f];
//}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    UITableViewCell *cell = (UITableViewCell*) [[textField superview] superview];
    [debtorTablle scrollToRowAtIndexPath:[debtorTablle indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
@end
