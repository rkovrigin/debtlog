//
//  Options.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 15.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "Options.h"
#import "DBmanager.h"

@interface Options ()

@end

@implementation Options

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    DBmanager *db = [DBmanager getSharedInstance];
    currencySC.selectedSegmentIndex = [[db getStr:@"select min(id) from currency where def = 1"] intValue] - 1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0){
        cell = currencyCell;
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	NSArray *sectionArray;
	sectionArray =  [NSArray arrayWithObjects:@"Default currency", nil];
	
	return [sectionArray count];
	
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{
    NSInteger sectionAmount = 0;
    switch (section){
        case 0: sectionAmount = 1;
            break;
        default: break;
    }
    
    return sectionAmount;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *sectionHeader = nil;
	switch (section) {
        case 0: sectionHeader = @"Default currency";
            break;
        default:
            break;
    }
	return sectionHeader;
}
@end
