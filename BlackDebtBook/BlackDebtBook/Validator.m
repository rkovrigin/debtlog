//
//  Validator.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 28.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "Validator.h"

@implementation Validator

-(BOOL)validateString:(NSString*)line{
    BOOL valid = TRUE;
    NSCharacterSet * set = [NSCharacterSet characterSetWithCharactersInString:@"@#$%^&*()_-=+|\\/{}[]<>\\\"';:Ω≈ç√∫˜µ≤≥÷æ…¬˚∆˙©ƒ∂ßåœ∑´®†¥¨ˆøπ“‘«»`~"];
//    NSCharacterSet * goodSet = [[NSCharacterSet characterSetWithCharactersInString:@" ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮёйцукенгшщзхъфывапролджэячсмитьбюabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"] invertedSet];
    
    NSRange r = [line rangeOfCharacterFromSet:set];
    if (r.location != NSNotFound) {
        valid = FALSE;
    }

    
    return valid;
}

-(BOOL)validateInt:(NSString *)line{
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:line];
    BOOL valid = [alphaNums isSupersetOfSet:inStringSet];
    return valid;
}

@end
