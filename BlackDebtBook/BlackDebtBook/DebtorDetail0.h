//
//  DebtorDetail0.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 21.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBmanager.h"

@interface DebtorDetail0 : UITableViewController{
    IBOutlet UITableViewCell *nameCell;
    IBOutlet UITableViewCell *descriptCell;
    IBOutlet UITextField *nameField;
    IBOutlet UITextView *descriptField;
    DBmanager *db;
}

@end
