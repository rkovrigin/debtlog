//
//  DBmanager.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 30.07.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBmanager : NSObject{
    NSString *databasePath;
}

+(DBmanager*)getSharedInstance;
-(BOOL)createDB;
-(BOOL)saveData:(NSString*)name;
-(NSString*) findByRegisterNumber:(NSString*)registerNumber;
-(NSMutableArray*) getIDs:(NSString*)tableName;
-(NSMutableArray*) getDebtIDs:(NSString*)tableName withdebtorID:(NSString*)debtorID;
-(NSMutableArray*) getRawWithQuery:(NSString*)query;
-(int)getTableSize:(NSString*)tableName;
-(NSString*)getStr:(NSString*)query;
-(void)Exec:(NSString*)query;

@end
