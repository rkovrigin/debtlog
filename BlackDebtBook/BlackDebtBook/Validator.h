//
//  Validator.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 28.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validator : NSObject

-(BOOL)validateString:(NSString*)line;
-(BOOL)validateInt:(NSString*)line;
@end
