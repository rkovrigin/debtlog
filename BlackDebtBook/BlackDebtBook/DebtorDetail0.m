//
//  DebtorDetail0.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 21.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "DebtorDetail0.h"
#import "DBmanager.h"
#import "Validator.h"

@interface DebtorDetail0 ()

@end

@implementation DebtorDetail0

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(addDebtor:)];
//    self.title = @"Debtor data";
    self.title = NSLocalizedString(@"TITLE_DEBTOR", nil);

}

- (void)addDebtor:(id)sender
{
    db = [DBmanager getSharedInstance];
    Validator *validator = [[Validator alloc] init];
    
    if (![validator validateString:nameField.text] || [nameField.text  isEqual: @""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"INVALID_NAME", nil) message:NSLocalizedString(@"CHAR_NUMERIC_NOT_EMPTY", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if (![validator validateString:descriptField.text]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"INVALID_DESCRIPTION", nil) message:NSLocalizedString(@"CHAR_NUMERIC", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    } else {
        NSString *newDebtor = [NSString stringWithFormat:@"insert into debtor (name, descript) values ('%@', '%@')", nameField.text, descriptField.text];
        NSLog(@"new %@", newDebtor);
        [db Exec:newDebtor];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    
//    // Configure the cell...
//    
//    return cell;
    UITableViewCell *cell = nil;
    if (indexPath.section == 0){
        cell = nameCell;
    }else{
        cell = descriptCell;
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	NSArray *sectionArray;
	sectionArray =  [NSArray arrayWithObjects:@"Name", @"Description", nil];
	
	return [sectionArray count];
	
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{
    NSInteger sectionAmount = 0;
    switch (section){
        case 0: sectionAmount = 1;
            break;
        case 1: sectionAmount = 1;
            break;
        default: break;
    }
    
    return sectionAmount;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *sectionHeader = nil;
	switch (section) {
        case 0: sectionHeader = NSLocalizedString(@"GROUP_NAME", nil);
            break;
        case 1: sectionHeader = NSLocalizedString(@"GROUP_DESCRIPTION", nil);
            break;
        default:
            break;
    }
	return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tv heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if( indexPath.section == 0 )
    {
        height = 44;
    } else {
        height = 170;
    }
    return height;
}

@end
