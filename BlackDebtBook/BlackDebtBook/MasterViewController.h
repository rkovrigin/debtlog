//
//  MasterViewController.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 14.02.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBmanager.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>{
    IBOutlet UITableViewCell *debtorName;
    IBOutlet NSMutableArray *debtIDs;
    IBOutlet NSMutableArray *currencyIDs;
    IBOutlet UITableViewCell *currencyCell;
    IBOutlet UISegmentedControl *currencySC;
    IBOutlet NSMutableArray *_debtors;
    DBmanager *db;
}

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
