//
//  DebtDetail0.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 21.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRLabel.h"
#import "DBmanager.h"

@interface DebtDetail0 : UITableViewController{
    
    IBOutlet UITableViewCell *dealTypeCell;
    IBOutlet UITableViewCell *currencyCell;
    IBOutlet UITableViewCell *amountCell;

    IBOutlet UITableViewCell *descriptCell;
    IBOutlet UITableViewCell *startDateCell;
    IBOutlet UITableViewCell *returnDateCell;
    IBOutlet UISegmentedControl *dealTypeNC;
    IBOutlet UISegmentedControl *currencyNC;
    IBOutlet UITextField *amountField;
    IBOutlet PRLabel *startDateLabel;
    IBOutlet PRLabel *returnDateLabel;
    IBOutlet UITextView *descriptText;
    
    IBOutlet NSMutableArray *dsections;
    IBOutlet NSMutableArray *dealTypeSections;
    IBOutlet NSDateFormatter *dateFormatDB;
    IBOutlet NSDateFormatter *dateFormatLabel;
    DBmanager *db;
}
@property NSString *debtID;
@property NSString *debtorID;
@property BOOL editable;

- (id)initWithID:(NSString *)_debtorID withDebtId:(NSString *)_debtID withIsEditable:(BOOL) isEditable;

@end
