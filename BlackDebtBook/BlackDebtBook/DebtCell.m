//
//  DebtCell.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 07.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "DebtCell.h"

@implementation DebtCell
@synthesize amount;
@synthesize startdata;
//@synthesize returndate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
