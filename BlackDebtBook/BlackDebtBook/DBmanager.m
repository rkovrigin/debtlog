//
//  DBmanager.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 30.07.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "DBmanager.h"

static DBmanager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBmanager

+(DBmanager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
//    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"debtbook.db"]];
    databasePath = [NSString stringWithFormat:@"%@", [docsDir stringByAppendingPathComponent: @"debtbook.db"]];
    BOOL isSuccess = YES;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO){
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *debtor_table = "create table if not exists debtor (id integer primary key, name text, descript text)";
            
//            const char *debt_table = "CREATE TABLE if not exists debt (id integer primary key, amount int, debtor int, foreign key (debtor) references debtor(id))";
            const char *debt_table = "CREATE TABLE debt (id integer primary key, amount int, startdate date, finishdate date, description text, debtor int, currency int, lastmodify DATETIME, foreign key (debtor) references debtor(id), foreign key (currency) references currency(id))";
            const char *currency = "create table currency(id integer primary key, name text, def integer, segment integer)";
            
            if (sqlite3_exec(database, debtor_table, NULL, NULL, &errMsg) != SQLITE_OK){
                isSuccess = NO;
                NSLog(@"Failed to create table debtor");
            }
            
            if (sqlite3_exec(database, debt_table, NULL, NULL, &errMsg) != SQLITE_OK){
                isSuccess = NO;
                NSLog(@"Failed to create table debt");
            }
            
            if (sqlite3_exec(database, currency, NULL, NULL, &errMsg) != SQLITE_OK){
                isSuccess = NO;
                NSLog(@"Failed to create table debt");
            }
            
//здесь нужно проверять локаль, и ставить нужную валюту, если правильной валюты нет, то использовать крышечки
            sqlite3_exec(database, "insert into currency (name, def, segment) values (\"eur\", 0, 0)", NULL, NULL, &errMsg);
            sqlite3_exec(database, "insert into currency (name, def, segment) values (\"usd\", 0, 1)", NULL, NULL, &errMsg);
            sqlite3_exec(database, "insert into currency (name, def, segment) values (\"rub\", 1, 2)", NULL, NULL, &errMsg);
            sqlite3_exec(database, "insert into currency (name, def, segment) values (\"cny\", 0, 3)", NULL, NULL, &errMsg);
            sqlite3_exec(database, "insert into currency (name, def, segment) values (\"gbp\", 0, 4)", NULL, NULL, &errMsg);
            
            sqlite3_close(database);
            sqlite3_finalize(statement);
            return isSuccess;
        }
        else {
            sqlite3_finalize(statement);
            sqlite3_close(database);
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return isSuccess;
}

- (BOOL) saveData:(NSString*)name
{
    const char *dbpath = [databasePath UTF8String];
    NSLog(@"dbpath%s", dbpath);
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into debtor (name) values (\"%@\")", name];
        NSLog(@"insertSQL %@", insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE){
            sqlite3_finalize(statement);
            return YES;
        } else {
            NSLog(@"sqlite3_step(statement) is %d", sqlite3_step(statement));
            sqlite3_finalize(statement);
            return NO;
        }
        sqlite3_reset(statement);
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return NO;
}

- (NSString*) findByRegisterNumber:(NSString*)registerNumber
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *querySQL = [NSString stringWithFormat: @"select name from debtor where id=\"%@\"",registerNumber];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            if (sqlite3_step(statement) == SQLITE_ROW){
//                NSString *name = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                NSString *name = [NSString stringWithFormat:@"%s", (const char *) sqlite3_column_text(statement, 0)];
                sqlite3_finalize(statement);
                sqlite3_close(database);
                return name;
            }else{
                NSLog(@"Not found");
                sqlite3_finalize(statement);
                sqlite3_close(database);
                return nil;
            }
            sqlite3_reset(statement);
        }
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return nil;
}

-(int)getTableSize:(NSString*)tableName
{
    const char *dbpath = [databasePath UTF8String];
    NSInteger size = 0;
    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *querySQL = [NSString stringWithFormat: @"select count(*) from %@", tableName];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            if (sqlite3_step(statement) == SQLITE_ROW){
//                NSString *name = [[NSString alloc] initWithUTF8String:
//                                  (const char *) sqlite3_column_text(statement, 0)];
//                size = [[NSString stringWithFormat:@"%s", (const char *) sqlite3_column_text(statement, 0)] intValue];
                size = [[NSString stringWithFormat:@"%s", (const char *) sqlite3_column_text(statement, 0)] intValue];
            }else{
                NSLog(@"Not found");
//                return 0;
                size = 0;
            }
            sqlite3_reset(statement);
        }
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return size;
}

-(NSMutableArray*) getIDs:(NSString *)tableName
{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *resultArray = [[NSMutableArray alloc]init];

    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *querySQL = [NSString stringWithFormat: @"select id from %@", tableName];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
//                NSString *_id = [[NSString alloc] init];
//                _id = [[NSString alloc] initWithUTF8String:
//                                (const char *) sqlite3_column_text(statement, 0)];
                [resultArray addObject:[[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 0)]];
            }
            sqlite3_reset(statement);
        }
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return resultArray;
}

-(NSMutableArray*) getRawWithQuery:(NSString *)query
{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *resultArray = [[NSMutableArray alloc]init];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *querySQL = [NSString stringWithFormat: @"%@", query];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
//                NSString *_id = [[NSString alloc] init];
//                _id = [[NSString alloc] initWithUTF8String:
//                       (const char *) sqlite3_column_text(statement, 0)];
                [resultArray addObject:[[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 0)]];
            }
            sqlite3_reset(statement);
        }
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return resultArray;
}

-(NSMutableArray*) getDebtIDs:(NSString *)tableName withdebtorID:(NSString *)debtorID
{
    const char *dbpath = [databasePath UTF8String];
    NSMutableArray *resultArray = [[NSMutableArray alloc]init];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
        NSString *querySQL = [NSString stringWithFormat: @"select id from %@ where debtor = %@", tableName, debtorID];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
//                NSString *_id = [[NSString alloc] init];
//                _id = [[NSString alloc] initWithUTF8String:
//                       (const char *) sqlite3_column_text(statement, 0)];
                [resultArray addObject:[[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 0)]];
            }
            sqlite3_reset(statement);
        }
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return resultArray;
}

-(NSString*) getStr:(NSString*)query{
    const char *dbpath = [databasePath UTF8String];
//    NSString *result = [NSString alloc];
    NSString *result;
    
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
        if(sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                result = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
//                result = [NSString stringWithFormat:@"%s", (const char *) sqlite3_column_text(statement, 0)];
            }
            sqlite3_reset(statement);
        }
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return result;
}

-(void) Exec:(NSString*)query{
    const char *dbpath = [databasePath UTF8String];
    NSLog(@"dbpath %s", dbpath);
    if(sqlite3_open(dbpath, &database) == SQLITE_OK){
        const char *query_stmt = [query UTF8String];
        if (sqlite3_exec(database, query_stmt, NULL, NULL, NULL) != SQLITE_OK){
//            NSLog(@"DELETE DON'T");
        }else{
//            NSLog(@"DELETE OK");
        }
        sqlite3_close(database);
    }
}

@end
