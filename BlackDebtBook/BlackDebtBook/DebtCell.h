//
//  DebtCell.h
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 07.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebtCell : UITableViewCell

@property (weak, nonatomic)   IBOutlet UILabel *amount;
@property (weak, nonatomic)   IBOutlet UILabel *startdata;
@property (weak, nonatomic)   IBOutlet UILabel *returndate;
//+(UITableViewCell *) cellFromNibNamed:(NSString *)nibName;
@end
