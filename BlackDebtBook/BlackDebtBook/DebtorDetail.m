//
//  DebtorDetail.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 21.08.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "DebtorDetail.h"

@interface DebtorDetail ()

@end
@implementation DebtorDetail
//@synthesize DebtoName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [DebtoName becomeFirstResponder];
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(addDebtor:)];
    self.title = @"Debtor data";
    // Do any additional setup after loading the view from its nib.
}

- (void)addDebtor:(id)sender
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    if (indexPath.section == 0){
        cell = dName;
    }else{
        cell = dCharact;
    }
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tv heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if( indexPath.section == 0 )
    {
        height = 44;
    } else {
        height = 170;
    }
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	NSArray *sectionArray;
	sectionArray =  [NSArray arrayWithObjects:@"Name", @"Description", nil];
	
	return [sectionArray count];
	
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{
    NSInteger sectionAmount = 0;
    switch (section){
        case 0: sectionAmount = 1;
            break;
        case 1: sectionAmount = 1;
            break;
        default: break;
    }
    
    return sectionAmount;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *sectionHeader = nil;
	switch (section) {
        case 0: sectionHeader = @"Name";
            break;
        case 1: sectionHeader = @"Description";
            break;
        default:
            break;
    }
	return sectionHeader;
}

@end
