//
//  DebtDetail0.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 21.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "DebtDetail0.h"
#import "AppDelegate.h"
#import "Validator.h"

@interface DebtDetail0 ()

@end

@implementation DebtDetail0
@synthesize debtID;
@synthesize debtorID;
@synthesize editable;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
//    self.editable = YES;
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    db = [DBmanager getSharedInstance];
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate * currentDate = [NSDate date];
    NSDateComponents * comps = [[NSDateComponents alloc] init];
    [comps setYear: +100];
    [comps setYear: 0];
    NSDate * minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
    [dateComponents setMonth:1];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:minDate options:0];
    
    if (self.debtID == nil){
        startDateLabel.text = [dateFormat stringFromDate: minDate];
        returnDateLabel.text = [dateFormat stringFromDate: newDate];
        currencyNC.selectedSegmentIndex = [[db getStr:@"select segment from currency where def = '1'"] intValue];
    }else{
        NSDate *tmpStart = [dateFormatDB dateFromString:[db getStr:[NSString stringWithFormat:@"select startdate from debt where id = %@", self.debtID]]];
        startDateLabel.text = [dateFormatLabel stringFromDate:tmpStart];
        NSDate *tmpReturn = [dateFormatDB dateFromString:[db getStr:[NSString stringWithFormat:@"select finishdate from debt where id = %@", self.debtID]]];
        returnDateLabel.text = [dateFormatLabel stringFromDate:tmpReturn];
        currencyNC.selectedSegmentIndex = [[db getStr:[NSString stringWithFormat:@"select currency from debt where id = %@", self.debtID]] intValue];
        int tmpAmount = [[db getStr:[NSString stringWithFormat:@"select amount from debt where id = %@", self.debtID]] intValue];
        if (tmpAmount >= 0){
            dealTypeNC.selectedSegmentIndex = 1;
        }else{
            dealTypeNC.selectedSegmentIndex = 0;
        }
        amountField.text = [NSString stringWithFormat:@"%d", abs(tmpAmount)];
        descriptText.text = [db getStr:[NSString stringWithFormat:@"select description from debt where id = %@", self.debtID]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    DBmanager *db = [DBmanager getSharedInstance];
    
    dateFormatDB = [[NSDateFormatter alloc] init];
    [dateFormatDB setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    dateFormatLabel = [[NSDateFormatter alloc] init];
    [dateFormatLabel setDateFormat:@"dd MMM yyyy"];
    
    if (self.editable == YES){
        self.navigationItem.rightBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveDebt:)];
    }else{
        self.navigationItem.rightBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editDebt:)];
    }
    
//    amountField.keyboardType = UIKeyboardTypeNumberPad;
}

- (IBAction)doneEditing:(id)sender {
    //    [StartDate resignFirstResponder];
}

- (void)saveDebt:(id)sender
{
//    DBmanager *db = [DBmanager getSharedInstance];
    Validator *validator = [[Validator alloc] init];
    
    NSDateFormatter *dateFormatToDB = [[NSDateFormatter alloc] init];
    [dateFormatToDB setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSDateFormatter *dateFormatFromLabel = [[NSDateFormatter alloc] init];
    [dateFormatFromLabel setDateFormat:@"dd MMM yyyy"];
    
    NSDate * currentDate = [NSDate date];
    NSDate *_startDate = [dateFormatFromLabel dateFromString:startDateLabel.text];
    NSDate *_returnDate = [dateFormatFromLabel dateFromString:returnDateLabel.text];
    
    NSString *_amount = [[NSString alloc] init];
    
    if (dealTypeNC.selectedSegmentIndex == 0){
        _amount = [NSString stringWithFormat:@"-%@", amountField.text];
    }else{
        _amount = amountField.text;
    }
    
    NSString *query = [[NSString alloc] init];

    
    if (self.debtID == NULL){
        query = [NSString stringWithFormat:@"insert into debt (amount, startdate, finishdate, description, debtor, currency, lastmodify) values ('%@', '%@', '%@', '%@', '%@', '%ld', '%@')",
                 _amount,
                 [dateFormatToDB stringFromDate:_startDate],
                 [dateFormatToDB stringFromDate:_returnDate],
                 descriptText.text,
                 self.debtorID,
                 (long)currencyNC.selectedSegmentIndex,
                 [dateFormatToDB stringFromDate: currentDate]];
    }else{
        query = [NSString stringWithFormat:@"update debt set amount = '%@', startdate = '%@', finishdate = '%@', description = '%@', debtor = '%@', currency = '%ld', lastmodify = '%@' where id = %@",
                 _amount,
                 [dateFormatToDB stringFromDate:_startDate],
                 [dateFormatToDB stringFromDate:_returnDate],
                 descriptText.text,
                 self.debtorID,
                 (long)currencyNC.selectedSegmentIndex,
                 [dateFormatToDB stringFromDate: currentDate],
                 self.debtID];
    }
    
    if (![validator validateInt:amountField.text]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"INVALID_AMOUNT", nil) message:NSLocalizedString(@"NUMERIC", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if ([amountField.text isEqual: @""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"INVALID_AMOUNT", nil) message:NSLocalizedString(@"NOT_EMPTY", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if (![validator validateString:descriptText.text]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"INVALID_DESCRIPTION", nil) message:NSLocalizedString(@"CHAR_NUMERIC", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else{
        NSLog(@"query %@", query);
        [db Exec:query];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if ([validator validateInt:amountField.text] && ![amountField.text isEqual: @""]){
    }else{
    }
}

- (void)editDebt:(id)sender
{
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    DebtDetail0 *debtdetail = [[DebtDetail0 alloc] initWithID:self.debtorID withDebtId:self.debtID withIsEditable:YES];
    [delegate.navigationController pushViewController:debtdetail animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0){
        cell = dealTypeCell;
    }else if (indexPath.section == 1){
        cell = currencyCell;
    }else if (indexPath.section == 2){
        if (indexPath.row == 0){
            cell = amountCell;
        }else if (indexPath.row == 1){
            cell = startDateCell;
        }else if (indexPath.row == 2){
            cell = returnDateCell;
        }else{
            cell = nil;
        }
    }else if(indexPath.section == 3){
        cell = descriptCell;
    }
    
    if (self.editable == YES){
        tableView.userInteractionEnabled = true;
        cell.userInteractionEnabled = true;
    }else{
//        tableView.userInteractionEnabled = false;
        cell.userInteractionEnabled = false;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tv heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if ( indexPath.section == 3 ){
        height = 145;
    }else if (indexPath.section == 0 || indexPath.section == 1){
        height = 29;
    }else{
        height = 44;
    }
    
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	NSArray *sectionArray;
	sectionArray =  [NSArray arrayWithObjects:@"Deal type", @"Deal", @"Currency", @"Description", nil];
	
	return [sectionArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//	//DEBT INFO
//    "DEAL_TYPE" = "DEAL TYPE";
//    "CURRENCY" = "CURRENCY";
//    "DEAL_DATA" = "DEAL DATA";
//    "DESCRIPTION" = "DESCRIPTION";
    
	NSString *sectionHeader = nil;
	switch (section){
        case 0: sectionHeader = NSLocalizedString(@"DEAL_TYPE", nil);
            break;
        case 1: sectionHeader = NSLocalizedString(@"CURRENCY", nil);
            break;
        case 2: sectionHeader = NSLocalizedString(@"DEAL_DATA", nil);
            break;
        case 3: sectionHeader = NSLocalizedString(@"DESCRIPTION", nil);
            break;
        default: break;
    }
	return sectionHeader;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{
    switch (section){
        case 0: return 1;
            break;
        case 1: return 1;
            break;
        case 2: return 3;
            break;
        case 3: return 1;
            break;
        default:
            break;
    }
    
    return 0;
}

- (id)initWithID:(NSString *)_debtorID withDebtId:(NSString *)_debtID withIsEditable:(BOOL) isEditable;{
    if ( (self = [super init]) ) {
    }
    self.debtorID = _debtorID;
    self.debtID = _debtID;
    self.editable = isEditable;    
    return self;
}
@end
