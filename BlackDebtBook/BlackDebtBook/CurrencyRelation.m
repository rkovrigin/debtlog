//
//  CurrencyRelation.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 15.09.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "CurrencyRelation.h"
#import "DBmanager.h"
#import "AppDelegate.h"
#import "DebtDetail0.h"

@interface CurrencyRelation ()

@end

@implementation CurrencyRelation
@synthesize curID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    db = [DBmanager getSharedInstance];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithID:(NSString *)currencyID {
    if ( (self = [super init]) ) {
        self.curID = currencyID;
    }
    db = [DBmanager getSharedInstance];
    NSString *curName = [db getStr: [NSString stringWithFormat:@"select name from currency where segment = %@", self.curID]];
    
    self.title = [NSString stringWithFormat:@"%@", NSLocalizedString(curName, nil)];
    
    return self;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    db = [DBmanager getSharedInstance];
    debts = [db getRawWithQuery:[NSString stringWithFormat:@"select id from debt where currency = '%@'", self.curID]];
    debtors = [db getRawWithQuery:[NSString stringWithFormat:@"select distinct debtor from debt where currency = '%@'", self.curID]];
    
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    if (indexPath.section == 0){
        NSString *amount = [db getStr:[NSString stringWithFormat:@"select sum(amount) from debt where debtor = %@ and currency = %@", [debtors objectAtIndex:indexPath.row], self.curID]];
        NSString *name = [db getStr:[NSString stringWithFormat:@"select name from debtor where id = %@", [debtors objectAtIndex:indexPath.row]]];
        if ([amount intValue] > 0){
            cell.textLabel.text = [NSString stringWithFormat:@"%@: %@ %d", name, NSLocalizedString(@"RETURN_FROM_YOU", nil), abs([amount intValue])];
        }else if ([amount intValue] < 0){
            cell.textLabel.text = [NSString stringWithFormat:@"%@: %@ %d", name, NSLocalizedString(@"RETURN_TO_YOU", nil), abs([amount intValue])];
        }else{
            cell.textLabel.text = [NSString stringWithFormat:@"%@: %d", name, abs([amount intValue])];
        }
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	NSArray *sectionArray;
	sectionArray =  [NSArray arrayWithObjects:@"Default currency", nil];
	
	return [sectionArray count];
	
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{
    db = [DBmanager getSharedInstance];
    debtors = [db getRawWithQuery:[NSString stringWithFormat:@"select distinct debtor from debt where currency = '%@'", self.curID]];
    NSInteger sectionAmount = [debtors count];
    return sectionAmount;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *sectionHeader = nil;
    if (section == 0){
        sectionHeader = NSLocalizedString(@"CURRENCY_TOTAL", nil);
    }

	return sectionHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)viewWillAppear:(BOOL)animated {
    db = [DBmanager getSharedInstance];
    debts = [db getRawWithQuery:[NSString stringWithFormat:@"select id from debt where currency = '%@'", self.curID]];
    debtors = [db getRawWithQuery:[NSString stringWithFormat:@"select distinct debtor from debt where currency = '%@'", self.curID]];
    [super viewWillAppear:animated];
    [self.debtsTable reloadData];
}

@end
