//
//  DetailViewController.m
//  BlackDebtBook
//
//  Created by Roman Kovrigin on 14.02.13.
//  Copyright (c) 2013 rkovrigin co. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import "DebtDetail.h"
#import "DebtDetail0.h"
#import "DebtCell.h"


@interface DetailViewController ()
- (void)configureView;
@end

@implementation DetailViewController
@synthesize bottomToolbar;
#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
//    if (_detailItem != newDetailItem) {
//        _detailItem = newDetailItem;
    
        // Update the view.
        [self configureView];
//    }
}

- (void)configureView
{
    // Update the user interface for the detail item.
//
//    if (self.detailItem) {
//        self.detailDescriptionLabel.text = [self.detailItem description];
//    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
    UIBarButtonItem *addBottomButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addDebt:)];
    
    self.navigationController.toolbar.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = addBottomButton;
    
    db = [DBmanager getSharedInstance];
    debts = [db getDebtIDs:@"debt" withdebtorID:self.debtorID];
    currency = [db getRawWithQuery: [NSString stringWithFormat:@"select distinct currency from debt where debtor = %@", self.debtorID]];
    [self configureView];
}

- (void)addDebt:(id)sender
{
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    DebtDetail0 *debtdetail = [[DebtDetail0 alloc] initWithID:self.debtorID withDebtId:nil withIsEditable:YES];
    [delegate.navigationController pushViewController:debtdetail animated:YES];
}

//- (void)editButtonItem:(id)sender
//{
//
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Debt", @"Debt");
    }
    return self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebtCell *cell = (DebtCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    db = [DBmanager getSharedInstance];
    currency = [db getRawWithQuery: [NSString stringWithFormat:@"select distinct currency from debt where debtor = %@", self.debtorID]];
    
    if( nil == cell ) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DebtCell"owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    switch (indexPath.section) {
        case 0:
        {
            NSDateFormatter *dateFormatDB = [[NSDateFormatter alloc] init];
            [dateFormatDB setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
            NSDateFormatter *dateFormatLabel = [[NSDateFormatter alloc] init];
            [dateFormatLabel setDateFormat:@"dd MMM yyyy"];

            NSInteger money = 0;
            NSString *cur = [db getStr:
                             [NSString stringWithFormat:@"select c.name from currency c left join debt d on c.segment = d.currency where d.id = %@ and d.debtor = %@",
                              [debts objectAtIndex:indexPath.row], self.debtorID]];
            money = [[db getStr:
                      [NSString stringWithFormat:@"select amount from debt where id = %@ and debtor = %@",
                       [debts objectAtIndex:indexPath.row], self.debtorID]] integerValue];
//            cell.amount.text = [NSString stringWithFormat:@"%d %@", money, cur];
//            NSLocalizedString(@"DEBT_RESULT", nil);
            NSString *_symbol = @"";
            if (money > 0){
                _symbol = @"+";
            }
            cell.amount.text = [NSString stringWithFormat:@"%@%d %@", _symbol, money, NSLocalizedString(cur, nil)];
            NSDate *_startdate = [dateFormatDB dateFromString:
                                  [db getStr:
                                   [NSString stringWithFormat:@"select startdate from debt where id = %@ and debtor = %@",
                                    [debts objectAtIndex:indexPath.row], self.debtorID]]];
            cell.startdata.text = [dateFormatLabel stringFromDate:_startdate];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
            break;
        case 1:{
            if([debts count] != 0){
                NSString *sum = [db getStr:[NSString stringWithFormat:@"select sum(amount) from debt where debtor = %@ and currency = %@", self.debtorID, [currency objectAtIndex:indexPath.row]]];
                NSString *cur = [db getStr:[NSString stringWithFormat:@"select name from currency where segment = %@", [currency objectAtIndex:indexPath.row]]];
//                cell.amount.text = @"Total";
//                cell.amount.text = NSLocalizedString(@"RESULT", nil);
//                cell.startdata.text = [NSString stringWithFormat:@"%@ %@", sum, NSLocalizedString(cur, nil)];
                cell.amount.text = nil;
                cell.startdata.text = nil;
                NSString *relation = @"";
                if ([sum intValue] > 0){
                    relation = NSLocalizedString(@"RETURN_FROM_YOU", nil);
                }else if ([sum intValue] < 0){
                    relation = NSLocalizedString(@"RETURN_TO_YOU", nil);
                }
                cell.textLabel.text = [NSString stringWithFormat:@"%@ %d %@", relation, abs([sum intValue]), NSLocalizedString(cur, nil)];
                cell.textLabel.textAlignment = NSTextAlignmentCenter;
            }else{
//                cell.amount.text = [NSString stringWithFormat:@"Empty"];
                cell.textLabel.text = [NSString stringWithFormat:@"Empty"];
            }
        }
            break;
        default:
            break;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    cell.backgroundColor = [UIColor redColor];
}

- (id)initWithID:(NSString *)debtorid {
    if ( (self = [super init]) ) {
        self.debtorID = debtorid;
    }
    db = [DBmanager getSharedInstance];
    self.title = [db getStr: [NSString stringWithFormat:@"select name from debtor where id = %@", self.debtorID]];
 
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *sectionHeader = nil;
	switch (section){
        case 0: sectionHeader = NSLocalizedString(@"DEBT_LOG", nil);
            break;
        case 1: sectionHeader = NSLocalizedString(@"DEBT_RESULT", nil);
            break;
        default: break;
    }
	return sectionHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    db = [DBmanager getSharedInstance];
    
    switch (section){
        case 0:
        {
            NSString *a = [db getStr : [NSString stringWithFormat : @"select count(*) from debt where debtor = %@", self.debtorID]];
            return [a intValue];
        }
            break;
        case 1:
        {
            NSString *a = [db getStr : [NSString stringWithFormat : @"select count(distinct currency) from debt where debtor = %@", self.debtorID]];
            return [a intValue];
        }
            break;
        default:
            break;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    db = [DBmanager getSharedInstance];
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    NSMutableArray *ids = [db getRawWithQuery:[NSString stringWithFormat:@"select id from debt where debtor = %@", self.debtorID]];
        
    if (indexPath.section == 0){
        DebtDetail0 *debtdetail = [[DebtDetail0 alloc] initWithID:self.debtorID withDebtId:[ids objectAtIndex:indexPath.row] withIsEditable:NO];
        [delegate.navigationController pushViewController:debtdetail animated:YES];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    db = [DBmanager getSharedInstance];
    debts = [db getDebtIDs:@"debt" withdebtorID:self.debtorID];
    [super viewWillAppear:animated];
    [self.debtsTable reloadData];
}
@end
